This plaintext version is generated from platform.html
using html2text, git tag 5.0.1.


Jonathan Carter
DPL Platform
2023-03-14


jcc@debian.org
https://jonathancarter.org
https://wiki.debian.org/highvoltage



1. Introduction

Hi, my name is Jonathan Carter, also known as highvoltage, with the Debian
account name of jcc, and I'm running for a fourth DPL term.
Serving as DPL for 3 terms has been somewhat of a roller coaster, full of
challenges and achievements for the project, including a pandemic that
affected all of us in so many ways, the Debian 11 (bullseye) release (which
turned out great!), 4 general resolutions, and 64 Debian Developers that have
joined (or re-joined) the project as members.

2. Why I'm running for DPL (yet again)

When I was first elected, I aimed to bring a sense of stability and normality
to the project, after we've been through a few contentious issues in the years
before. I knew this was going to be tough, since some wounds ran deep, and
beyond Debian, many things in the world are simply beyond our control.
I believe that really good progress has been made on making Debian a much more
stable project, and many people have worked really hard for us to achieve
this, but the reason why I want stability is not just for its own sake.
Stability brings with it safety and space to grow, and I believe that Debian
is at a stage now where it's ready to grow and flourish in to something more.
There's probably nothing that can prepare you more for a DPL term than having
served a few DPL terms. I understand Debian really well, I understand our
challenges well, I've learned how to better work with people within our
project, all of which helped in identifing and clearing blockers, and while I
believe there are plenty of people in Debian who could be a good DPL, I
believe that my skills, experience, patience and understanding bring together
a great blend to help guide Debian through the next year.

3. Agenda


3.1. Help Debian to shine on its own in the GNU/Linux eco-system

Ever heard the saying "Debian is the distro that other distros are based on"?
It happens to be a very factual statement, there's a bunch of very prominent
systems that are based on Debian, but surely Debian shouldn't only be that. In
my personal life, I use Debian on my desktops at home, servers, media players
and even for those same things at work. A long time ago I learned some basics
about how to set up Debian, and since then, I don't want to imagine a world
where I don't have Debian anymore. It's a great general purpose operating
system (and for those playing DPL bingo, here you go... some might even call
it a Universal Operating System!). But that is not the perception that exists
in the world out there. Recently I overheard someone explaining what Debian
is, calling it a "reference distro", implying that it's good, but that no one
directly uses it. In practice, it's not much different than saying "Debian is
the distro other distros are based on", but I'll admit it did sting a tiny bit
when I heard it phrased that way.
I believe that the issues that stand in the way for Debian being as popular as
it deserves can be fixed, and that the next cycle after our Debian 12 release
is an ideal time to make the hardest pushes in that direction that we've ever
made before.
3.1.1. Identify and fix top issues
For Debian 12, we've voted_to_include_non-free_firmware on the default
installation media. While non-free firmware is in itself still a big problem,
I consider this a huge step forward in making it easier for users to install
Debian on bare metal (especially since increasingly more laptops don't have
ethernet ports, and increasingly more servers need firmware for their ethernet
ports to function). I'm very happy that we've done this, it was likely the
number one thing standing between us and more adoption.
But I think we can, and should do more. I would like us to collectively
identify the top problems in Debian that give users a hard time, find
potential solutions for them, and if it seems plausible, make it a release
goal. I'll list two examples from my own personal suffering below, and they're
examples, so this DPL vote isn't a vote to override a maintainer in any way,
but I'm listing them to give you an idea of what I'm talking about.
On desktop systems, automatically run fsck when a filesystem encounters a
problem. On a very regular basis, a contact sends me an email or Signal
message containing a photo of their laptop/desktop/server with a busybox
prompt with a message above saying something like "filesystem check failed for
/dev/sda2". And then I get to be the hero who tells them "Type 'fsck -y /dev/
sda2', then reboot!". I can understand that on some servers, some might want
to be conservative and intervene manually, but on a desktop system the answer
to the user is almost always going to be to run the filesystem check, so why
not just do it automatically? The incident can still be reported to the user
in other ways after the filesystem is fixed.
Let dpkg resume automatically on desktop systems. Another common black screen
at boot problem happens when the display manager hasn't started up because
it's not finished being configured yet. In this case, the answer is nearly
always "Press ctrl+alt+f2, log in, type 'sudo dpkg --configure -a' and then
run 'apt upgrade' (or whatever was installing stuff) again. Whenever apt tells
you to run 'dpkg --configure -a', the answer on desktop systems are certainly
nearly always going to be to type that, so, why can't apt handle this
automatically for the user? (and, ideally we should have a mechanism that
checks whether this is necessary at boot time).
Those are just two of my pet peeves, but I'd like us a project to identify and
agree to fix some of the top problems that regularly affect our users.
Sometimes issues affecting our users are tough on them, but would be
relatively trivial to fix. We shouldn't let those kind of bugs continue to
live for yet another release.
3.1.2. Make Debian exciting for the community. We have a large community of
users, casual contributors and developers alike. We have problems in all of
those cases. For users, we really need to make local groups happen. I've
identified this before, although the pandemic got in the way of expanding that
idea. I think now is the time to formalise more support for local groups since
they help distribute marketing material, create hype, provide support, and
sometimes, when we're really lucky, they even take on huge tasks like
submitting a DebConf bid. Local groups of users are essential to Debian, and
we need to support them more. For casual contributors, we need a better path
to help them become more involved in Debian. For debian developers, I think we
need to find more ways to support each other. I find too often that a
developer is slogging away for weeks (or even months) at end on problems
without much help, all for the greater good, and while it's fun to be the
hero, I'd like to see more processes and channels for developer support than
just filing an RFH bug.
3.1.3. Reduce waiting time on NEW queue. Firstly, I'd like to thank the FTP
team for their heroics during the last two cycles. Whenever it was crunch
time, they delivered in shortening NEW. However, their bandwidth is very
finite, and at times the waiting times were much longer than many were
comfortable with. One of the proposals in the past was to make packages in the
queue public, so that it's easier for a casual observer to point out problems
and do an easy reject (reducing waiting time for the developer, and freeing
some time for FTP team reviewers). From what I understand, the issue is that
FTP team is concerned with potential copyright problems in uploads since some
people might treat it as some form of package archive. During the last three
years, we've signed a few retainers with lawyers who can help us with
copyright issues. I hope that the FTP team will be open to speaking to a
copyright lawyer who could give us some more guidance on this. So far we
publish packages on mentors.debian.net and it hasn't been a problem, but I
think if we could get some solid legal advice instead of just speculating,
then it could put some minds at ease.

3.2. Funding development

3.2.1. Outreach. So far, 2023 is our worst year in recent years when it comes
to outreach. We've been depending mostly on Google Summer of Code and
Outreachy for outreach, and having these external organisations to help manage
this has been great. This year, we weren't selected for GSoC since our project
list was too small. On the Outreachy side, they've become quite expensive.
I'll post more about this to the debian-project list in the coming weeks, but
the plan is to revoke our current outreach delegation and replace it with a
new team of DDs who will build an internal outreach program where we can
likely make better use of the same amount of funds.
3.2.2. Grants. We should find ways to pay contributors for doing pieces of
well-defined work. In the past, there's been an effort by a previous DPL to
pay Debian Developers, but unfortunately it was so poorly executed that future
DPLs would rather avoid the issue altogether. I believe that we should have a
system where we mark bug reports that we can't carry as volunteers with an
appropriate tag, and once a scope of work has been fleshed out for a report,
complete with a time estimate, then we could put some bounty out for it or
someone qualified can take a stab at solving it.

4. In Closing

Some of my thoughts above are purposely not too rigid or specific, my goal as
DPL is to enact the wishes of the project, not the other way around. Please
share your thoughts on the debian-vote list during this campaign_period so
that you can help shape the next DPL term.
Thank you for taking the time to read my platform. This year I'm running
unopposed, that means I have to work a bit harder to convince you to vote.
Please do go and vote!

A. Changelog

This platform is version controlled in a git_repository.

* 5.0.0: New platform for 2023 DPL elections.
* 5.0.1: Fix a few small errors.


