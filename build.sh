#!/bin/sh
#
# Dependencies: html2wml, html2text
#
# By Jonathan Carter <jcc@debian.org>
# I consider this script trivial and assert no copyright,

VERSION="5.0.1"

# Plain version for debian website:
cat wmlheader.html content.html > platform.wml

# More readable version for mine:
cat style.css > platform.html
sed -e "s/:##}//g" content.html >> platform.html

# Plaintext version:
echo "This plaintext version is generated from platform.html" > README.md
echo "using html2text, git tag $VERSION." >> README.md
html2text -style pretty -width 78 platform.html | tail -n +3 >> README.md
